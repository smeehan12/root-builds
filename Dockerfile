ARG BASE_IMAGE=python:3.8-slim
FROM ${BASE_IMAGE} as base

ARG TARGET_BRANCH=v6-18-02
ARG GIT_PROJECT_URL=https://github.com/root-project/root

SHELL [ "/bin/bash", "-c" ]

COPY packages.txt packages.txt
RUN apt-get -qq -y update && \
    apt-get -qq -y install --no-install-recommends $(cat packages.txt) && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/*
# c.f. https://root.cern.ch/building-root#options
RUN mkdir /code && \
    cd /code && \
    git clone --depth 1 "${GIT_PROJECT_URL}" \
      --branch "${TARGET_BRANCH}" \
      --single-branch \
      root_src && \
    cmake \
      -Dall=OFF \
      -Droofit=OFF \
      -Dminuit2=OFF \
      -DCMAKE_CXX_STANDARD=17 \
      -Dfortran=OFF \
      -Dpyroot=ON \
      -DPYTHON_EXECUTABLE=$(which python3) \
      -S root_src \
      -B build && \
    cmake --build build -- -j2

# Use C.UTF-8 locale to avoid issues with ASCII encoding
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV PYTHONPATH=/code/build/lib:$PYTHONPATH
ENV LD_LIBRARY_PATH=/code/build/lib:$LD_LIBRARY_PATH
ENV ROOTSYS=/code/build

WORKDIR /home/data
ENV HOME /home

ENTRYPOINT ["/bin/bash", "-l", "-c"]
CMD ["/bin/bash"]
